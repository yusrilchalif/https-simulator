import socket, sys
#import base64
from Crypto.PublicKey import RSA


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         
host = "localhost"
port = 1310

new_key = RSA.generate(4096, e=65537)
private_key = new_key.exportKey("PEM")
public_key = new_key.publickey().exportKey("PEM")

#server.send(public_key)

#key = get_random_bytes(16)
#key = RSA.generate(1024, random_generator)
#public_key = key.publickey()
#server.send(public_key)

#make generate key

#key = Fernet.generate_key()
#cipher_suite = Fernet(key)
#cipher_text = cipher_suite.encrypt(b"This is a key")
#plain_text = cipher_suite.decrypt(cipher_text)

sock.bind(("localhost", port))        
f = open('torecv.png','wb')
sock.listen(5)                 
while True:
    c, addr = sock.accept()     
    print 'Got connection from', addr
    print "Receiving..."
    l = c.recv(1024)
    while (l):
        print "Receiving..."
        f.write(l)
        l = c.recv(1024)
    f.close()
    print "Done Receiving"
    c.send('Thank you for connecting')
    break
c.shutdown(socket.SHUT_WR)
c.close()
