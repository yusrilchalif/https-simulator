import socket, sys
from cryptography.fernet import Fernet

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         
host = "localhost"
port = 1310

server_public_key = sock.recv(1024).decode()
print public_key

key = Fernet.generate_key()
cipher_suite = Fernet(key)
cipher_text = cipher_suite.encrypt(b"A really secret message. Not for prying eyes.")
plain_text = cipher_suite.decrypt(cipher_text)
print key

sock.connect(("localhost", port))
sock.send("Hello server!")
f = open('tosend.png','rb')
print 'Sending...'
l = f.read(1024)
while (l):
    print 'Sending...'
    sock.send(l)
    l = f.read(1024)
f.close()
print "Done Sending"
sock.shutdown(socket.SHUT_WR)
print sock.recv(1024)
sock.close 
